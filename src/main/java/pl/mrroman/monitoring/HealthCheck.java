package pl.mrroman.monitoring;

import pl.mrroman.web.Request;
import pl.mrroman.web.Response;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

class HealthCheck {

    private final DataSource dataSource;

    HealthCheck(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    Response<String> handleRequest(Request<String> request) {
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                statement.execute("SELECT 1");
                return new Response<>(200, "text/plain", "OK");
            }
        } catch (SQLException e) {
            return new Response<>(500,"text/plain", "Error: " + e.getMessage());
        }
    }
}
