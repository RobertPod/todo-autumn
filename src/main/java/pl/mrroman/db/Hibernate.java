package pl.mrroman.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

public class Hibernate {

    static {
        System.setProperty("org.jboss.logging.provider", "slf4j");
    }

    public static SessionFactory initSessionFactory(DataSource dataSource, Class<?>... klasses) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.connection.datasource", dataSource);
        properties.put("hibernate.hbm2ddl.auto", "create-drop");

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .build();

        MetadataSources metadata = new MetadataSources(serviceRegistry);
        Stream.of(klasses).forEach(metadata::addAnnotatedClass);

        return metadata
                .getMetadataBuilder()
                .build()
                .getSessionFactoryBuilder()
                .build();
    }

    public static <T,R> Function<T,R> transactionalFunction(SessionFactory sessionFactory, BiFunction<Session, T, R> function) {
        return (in) -> {
            try (final Session session = sessionFactory.openSession()) {
                Transaction transaction = session.beginTransaction();
                R out = function.apply(session, in);
                transaction.commit();
                return out;
            }
        };
    }

}
