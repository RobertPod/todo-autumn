package pl.mrroman.web;

public class Response<T> {
    private final int statusCode;
    private final String contentType;
    private final T content;

    public Response(int statusCode, String contentType, T content) {
        this.statusCode = statusCode;
        this.contentType = contentType;
        this.content = content;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getContentType() {
        return contentType;
    }

    public T getContent() {
        return content;
    }

}
