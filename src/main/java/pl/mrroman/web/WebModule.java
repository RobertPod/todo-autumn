package pl.mrroman.web;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.RoutingHandler;

import java.util.stream.Stream;

public class WebModule {

    private final RoutingHandler handler;
    private final int port;

    public WebModule(int port, RoutingHandler ...routingHandlers) {
        this.port = port;
        this.handler = Handlers
                .routing();

        Stream.of(routingHandlers).forEach(handler::addAll);
    }

    public Undertow start() {
        Undertow server = Undertow.builder()
                .setHandler(handler)
                .addHttpListener(port, "localhost")
                .build();

        server.start();
        return server;
    }

}
